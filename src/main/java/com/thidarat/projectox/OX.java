/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.projectox;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class OX {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Welcome to OX Game");
        char[][] board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }

        }

        boolean player1 = true;
        boolean gameEnded = false;

        while (!gameEnded) {
            
            drawBoard(board);
            if (player1) {
                System.out.println("Turn O:");
            } else {
                System.out.println("Turn X:");
            }
            

            char c = '-';
            if (player1) {
                c = 'O';
            } else {
                c = 'X';
            }

            int row = 0;
            int col = 0;

            while (true) {

                System.out.println("Please input row, col : ");
                row = in.nextInt();
                col = in.nextInt();

                if (row < 0 || col < 0 || row > 2 || col > 2) {
                    System.out.println("This position is off the bounds of the board! Try again.");

                } else if (board[row][col] != '-') {
                    System.out.println("Someone has already made a move at this position! Try again.");

                } else {
                    break;

                }

            }
            board[row][col] = c;

            if (playerHasWin(board) == 'O') {
                System.out.println("  O Win!");
                gameEnded = true;
            } else if (playerHasWin(board) == 'X') {
                System.out.println(" X Win!");
                gameEnded = true;
            } else {

                if (boardIsFull(board)) {
                    System.out.println("Draw");
                    gameEnded = true;
                } else {
                    //If player1 is true, make it false, and vice versa; this way, the players alternate each turn
                    player1 = !player1;
                }
            }

        }
        drawBoard(board);

    }

    public static void drawBoard(char[][] board) {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j]);
            }
            System.out.println();

        }
    }

    public static char playerHasWin(char[][] board) {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != '-') {
                return board[i][0];
            }
        }

        for (int j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != '-') {
                return board[0][j];
            }
        }
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-') {
            return board[0][0];
        }
        if (board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[2][0] != '-') {
            return board[2][0];
        }

        return ' ';

    }

    public static boolean boardIsFull(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

}
